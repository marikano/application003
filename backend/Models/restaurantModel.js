const mongoose = require("mongoose");

const Schema = mongoose.Schema;
const restaurantModel = new Schema({
  name: { type: String },
  formatted_address: { type: String }
});

module.exports = mongoose.model('Restaurant', restaurantModel);
