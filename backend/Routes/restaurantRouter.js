const express = require("express");
const Restaurant = require("../Models/restaurantModel");

const restaurantRouter = express.Router();

restaurantRouter.route('/')
  .get((req, res) => {
    Restaurant.find({}, (err, restaurant) => {
      res.json(restaurant)
    })
  })
  .post((req, res) => {
    const restaurant = new Restaurant(req.body);
    restaurant.save();
    res.status(201).send(restaurant);
  });

restaurantRouter.use('/:restaurantId', (req, res, next) => {
  console.log("Uses restaurantId")
  Restaurant.findById(req.params.restaurantId, (err, restaurant) => {
    if (err) res.status(500).send(err);
    else {
      req.restaurant = restaurant;
      next()
    }
  });
});
restaurantRouter.route('/:restaurantId')
  .get((req, res) => {
    res.json(req.restaurant);
  })
  .put((req, res) => {
    req.restaurant.name = req.body.title;
    req.restaurant.address = req.body.address;
    req.restaurant.save();
    res.json(req.restaurant);
  })
  .patch((req, res) => {
    if (req.params._id) {
      delete req.body._id;
    }
    for (const b in req.body) {
      req.restaurant[b] = req.body[b];
    }
    req.restaurant.save();
    res.json(req.restaurant);
  })
  .delete((req, res) => {
    req.restaurant.remove((err) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.status(204).send('Restaurant removed');
      }
    });
  });

module.exports = restaurantRouter;