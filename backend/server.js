const mongoose = require("mongoose");
const express = require("express");
const bodyParser = require("body-parser");
const restaurantRouter = require("./Routes/restaurantRouter");

const app = express();

const db = mongoose.connect('mongodb://test:testi123@ds253203.mlab.com:53203/dropit', { useNewUrlParser: true });


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('api/Restaurant', restaurantRouter);

app.get('/', (req, res) => {
  res.send('Hello World');
});

app.listen(8081, () => {
  console.log('Example app listening at http://localhost:8081');
});
